import { exercise1 } from "../implementation/Ex1.js";
import { exercise2 } from "../implementation/Ex2.js";
import { exercise3 } from "../implementation/Ex3.js";
import { exercise4 } from "../implementation/Ex4.js";
import { exercise5, arrToLinkedList } from "../implementation/Ex5.js";

export const exercises = [
  {
    fn: exercise1,
    description: "Add two numbers and return the sum",
    examples: [
      {
        input: [1, 2],
        expected: 3,
      },
      {
        input: [7, 0],
        expected: 7,
      },
    ],
  },
  {
    fn: exercise2,
    description: "Capitalize the longest word",
    examples: [
      {
        input: ["The sky is blue."],
        expected: "The sky is BLUE.",
      },
      {
        input: [
          "There are multiple words in this ordinary sentence that are eight letters long.",
        ],
        expected:
          "There are MULTIPLE words in this ORDINARY SENTENCE that are eight letters long.",
      },
    ],
  },
  {
    fn: exercise3,
    description: "Flatten the array",
    examples: [
      {
        input: [[1, 5, 7]],
        expected: [1, 5, 7],
      },
      {
        input: [[1, 2, [1, 2]]],
        expected: [1, 2, 1, 2],
      },
      {
        input: [[1, 2, []]],
        expected: [1, 2],
      },
      {
        input: [["one", 2, [3, "four"]]],
        expected: ["one", 2, 3, "four"],
      },
      {
        input: [
          [
            [1, 2, 3],
            [4, 5, []],
            [7, 8, [[9, [10]], 11], 12],
            [13, 14, ["foo", "bar"], 15],
          ],
        ],
        expected: [
          1,
          2,
          3,
          4,
          5,
          7,
          8,
          9,
          10,
          11,
          12,
          13,
          14,
          "foo",
          "bar",
          15,
        ],
      },
    ],
  },

  {
    fn: exercise4,
    description: "Find names with duplicate words",
    examples: [
      {
        input: ["./data/ex4data.csv"],
        expected: [
          "James Brown",
          "William Jones",
          "Christopher Perez",
          "William Thompson",
          "Elizabeth Ramirez",
          "Sarah Williams",
          "John Anderson",
          "Richard Thomas",
          "Jessica Ramirez",
          "Jessica Thomas",
          "Betty Johnson",
          "Mark Martinez",
          "Lisa Anderson",
          "Richard Wilson",
          "Nancy Martinez",
          "Thomas Lee",
          "Sarah Lewis",
          "Daniel Rodriguez",
        ],
      },
    ],
  },
  {
    fn: exercise5,
    description: "Merge two sorted linked lists",
    examples: [
      {
        input: [arrToLinkedList([1, 2, 4]), arrToLinkedList([1, 3, 4])],
        expected: arrToLinkedList([1, 1, 2, 3, 4, 4]),
      },
      {
        input: [arrToLinkedList([]), arrToLinkedList([1])],
        expected: arrToLinkedList([1]),
      },
      {
        input: [arrToLinkedList([1, 2]), arrToLinkedList([5])],
        expected: arrToLinkedList([1, 2, 5]),
      },
    ],
  },
];
