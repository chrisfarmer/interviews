# Javascript interview

## Goals

We're looking to get a feel for your proficiency and your thought process when faced with some short and unfamiliar javascript tasks. There are five exercises. For each of these, provide an implementation for the missing function.

## Setup

Run `npm install` to install a few dependencies.

## Exercises

Complete the implementation for each of the files in the implementation folder:

1. Ex1.js: Add two numbers
2. Ex2.js: Capitalize the longest words
3. Ex3.js: Flatten the array
4. Ex4.js: Merge two sorted linked lists
5. Ex5.js: Find rows with duplicate values

## Execution

To execute the exercises, run `node main.js`. Results will be output to the console.
