// Load the ex4data.csv data set.  It contains a "Name" field with a person's name,
// and also contains columns A, B, C, D, E, and F which contain random english words.
// Return an array that contains the names where that row contained the same word more than twice.
//
// For example, for this input file:
//
// Name, A, B, C, D
// Alice, green, red, blue, orange
// Bill, green, green, red, blue
// Charlie, red, green, blue, blue
//
// The return value should be:
//    [ "Bill", "Charlie" ]
//

import { readFileSync } from "fs";

export const exercise4 = (file) => {
  const csv = readFileSync(file, { encoding: "utf-8" });
  // csv contains the text of the csv file.  The file can be found in data/ex4data.csv.
};
