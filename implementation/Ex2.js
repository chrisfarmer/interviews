// Given an English language sentence, capitalize the longest word in that
// sentence.  Return a string that is the sentence with the longest word
// capitalized.  If there is more than one word that is the longest,
// capitalize all of them.
//
// Examples:
//   I sometimes love pizza.  ==> I SOMETIMES love pizza.
//   I forgot to add the butter to the batter in my cake. ==> I FORGOT to add the BUTTER to the BATTER in my cake.

export const exercise2 = (sentence) => {
  // return the transformed sentence.
};
