// Given an array "arr" that potentially contains other arrays
// as elements, return a new array that is a flattened version
// of that array.

// Examples:
//   [1, 2, 3] ==> [1, 2, 3]   (already flat)
//   [1, 2, [1, 2]] ==> [1, 2, 1, 2]
//   [1, 2, []] ==> [1, 2]    (ignore empties)
//   ["one", 2, [3, "four"]] ==> ["one", 2, 3, "four"]
//   [1, [2, 3, [4, 5], 1], [2, 3]] ==> [1, 2, 3, 4, 5, 1, 2, 3]

export const exercise3 = (arr) => {
  // return the new transformed array;
};
