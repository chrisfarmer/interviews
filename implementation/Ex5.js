// Merge two sorted linked lists into a new linked list, maintaining sort without using toArray function.
//
// LinkedList has a constructor that sets the head of the array which should be null for an empty list or a ListNode
//
// The LinkedList class has a few convenience methods:
//   - "toArray" function that enables conversion of the linked list to an array for checking answer
//   - "add" function which can be used add items to the end of the list.
//
// The ListNode class has two variables, val which is the value of the node and next which is the next node in the list or null for the final
// node
//
// Examples:
//   [1, 2, 4] and [1, 3, 4] ==> [1, 1, 2, 3, 4]
//   [] and [1] ==> [1]
//   [] and [] ==> []

export class ListNode {
  constructor(val, next = null) {
    /**
     * The numeric value of this list node
     * @type {number}
     */
    this.val = val;
    /**
     * Next node in the list, or null if this is the last node
     * @type {ListNode | null}
     */
    this.next = next;
  }
}

export class LinkedList {
  constructor(head = null) {
    this.head = head;
  }

  /**
   * Add a new node to the end of the current list.
   * If a ListNode, then the node gets added directly.
   * If a number, then a ListNode will get created with the number and then added.
   * @param {ListNode | number} valueOrNode
   */
  add(valueOrNode) {
    const newNode =
      typeof valueOrNode === "number"
        ? new ListNode(valueOrNode, null)
        : valueOrNode;
    if (this.head === null) {
      this.head = newNode;
    } else {
      let current = this.head;
      while (current.next) {
        current = current.next;
      }
      current.next = newNode;
    }
  }

  toArray() {
    var arr = [];
    var node = this.head;
    while (node != null) {
      arr.push(node.val);
      node = node.next;
    }
    return arr;
  }

  toString() {
    return JSON.stringify(this.toArray());
  }
}

export const arrToLinkedList = (arr) => {
  const list = new LinkedList();
  arr.forEach((element) => {
    list.add(element);
  });
  return list;
};

/**
 * Accepts two already-sorted linked lists and merges them to create a new sorted list, returning that merged list.
 * @param {LinkedList} list1
 * @param {LinkedList} list2
 * @returns LinkedList
 */
export const exercise5 = (list1, list2) => {
  // example usage of LinkedList
  //
  //   const newList = new LinkedList();
  //   newList.add(1);
  //   newList.add(2);
  //
  //   return newList;
  //
};
