import _ from "lodash";
import { exercises } from "./data/examples.js";

exercises.forEach((exercise, index) => {
  const { fn, description, examples } = exercise;
  console.log(``);
  console.log(`Exercise ${index + 1}: ${description}`);
  console.log(``);
  examples.forEach((example, exampleIndex) => {
    const { input, expected } = example;
    console.log(
      `    Example ${exampleIndex + 1} input: ${input
        .map((elem) => JSON.stringify(elem))
        .join(", ")}`
    );
    try {
      const result = fn(...input);
      if (result !== undefined) {
        console.log(`      Expected: ${JSON.stringify(expected)}`);
        console.log(`      Actual:   ${JSON.stringify(result)}`);
        if (
          (result.toArray && _.isEqual(result.toArray(), expected.toArray())) ||
          _.isEqual(result, expected)
        ) {
          console.log(`      CORRECT`);
        } else {
          console.log(`      INCORRECT`);
        }
      } else {
        console.log(`      Not implemented`);
      }
    } catch (ex) {
      console.log(`      Exception: ${ex.message}`);
      console.log(`    INCORRECT`);
    }
    console.log("");
  });
});
